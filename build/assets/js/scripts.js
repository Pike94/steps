'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}
// search page
function pageWidget(pages) {
  var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
  widgetWrap.prependTo("body");
  for (var i = 0; i < pages.length; i++) {
    if (pages[i][0] === '#') {
      $('<li class="widget_item"><a class="widget_link" href="' + pages[i] +'">' + pages[i] + '</a></li>').appendTo('.widget_list');
    } else {
      $('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
    }
  }
  var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:fixed;top:0;left:0;z-index:9999;padding:20px 20px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
  widgetStilization.prependTo(".widget_wrap");
}

function generateCode() {
  return Math.ceil(Math.random() * (+9999 - +1000) + +1000) + ' ' + Math.ceil(Math.random() * (+9999 - +1000) + +1000);
}

$(document).ready(function($) {
  pageWidget(['index']);
  $('input[name=code]').val(generateCode());
});

$(document).on('click', '.dropdown-selected', function(){
  $('.dropdown-menu').toggleClass('show');
});

$(document).on('click', '.call-menu', function(){
  $('.header-menu').addClass('active');
});
$(document).on('click', '.close-menu', function(){
  $('.header-menu').removeClass('active');
});

$(document).on('click', '.next-btn', function(){
  let thisModalId = $(this).parents('.modal').attr('id');

  switch (thisModalId) {
    case 'modal-1':
      if($('#'+thisModalId).find('input').val().length != 0){
        $('#modal-1').stop().animate().fadeOut();
        setTimeout(function(){
          $('.loader').addClass('active');
          setTimeout(function(){
            $('.loader').removeClass('active');
            $('#modal-2').stop().animate().fadeIn()
          }, 2500);
        }, 350);
        $('.step-btn:nth-child(1)').removeClass('active');
        $('.step-btn:nth-child(1)').addClass('visited');
        $('.step-btn:nth-child(2)').removeClass('disabled');
        $('.step-btn:nth-child(2)').addClass('active');
      }
      break;
    case 'modal-2':
      $('#modal-2').stop().animate().fadeOut();
      setTimeout(function(){
        $('.loader').addClass('active');
        setTimeout(function(){
          $('.loader').removeClass('active');
          $('#modal-3').stop().animate().fadeIn()
        }, 2500);
      }, 350);
      $('.step-btn:nth-child(2)').removeClass('active');
      $('.step-btn:nth-child(2)').addClass('visited');
      $('.step-btn:nth-child(3)').removeClass('disabled');
      $('.step-btn:nth-child(3)').addClass('active');
      break;
    case 'modal-3':
      $('#modal-3').stop().animate().fadeOut();
      setTimeout(function(){
        $('.loader').addClass('active');
        setTimeout(function(){
          $('.loader').removeClass('active');
          $('#modal-4').stop().animate().fadeIn()
        }, 2500);
      }, 350);
      $('.step-btn:nth-child(3)').removeClass('active');
      $('.step-btn:nth-child(3)').addClass('visited');
      $('.step-btn:nth-child(4)').removeClass('disabled');
      $('.step-btn:nth-child(4)').addClass('active');
      break;
    case 'modal-4':
      $('#modal-4').stop().animate().fadeOut();
      setTimeout(function(){
        $('.loader').addClass('active');
        setTimeout(function(){
          $('.loader').removeClass('active');
          $('#modal-5').stop().animate().fadeIn()
        }, 2500);
      }, 350);
      $('.step-btn:nth-child(4)').removeClass('active');
      $('.step-btn:nth-child(4)').addClass('visited');
      $('.step-btn:nth-child(5)').removeClass('disabled');
      $('.step-btn:nth-child(5)').addClass('active');
      break;
    case 'modal-5':
      $('#modal-5').stop().animate().fadeOut();
      setTimeout(function(){
        $('.loader').addClass('active');
        setTimeout(function(){
          $('.loader').removeClass('active');
          $('#modal-6').stop().animate().fadeIn()
          $('.progress-bar').addClass('active');
          setTimeout(function(){
            $('#modal-6').stop().animate().fadeOut();
            setTimeout(function(){
              $('#modal-7').stop().animate().fadeIn();
            }, 350);
          }, 3500);
        }, 2500);
      }, 350);
      $('.step-btn:nth-child(5)').removeClass('active');
      $('.step-btn:nth-child(5)').addClass('visited');
      $('.step-btn:nth-child(6)').removeClass('disabled');
      $('.step-btn:nth-child(6)').addClass('active');
      break;
    case 'modal-6':
      $('#modal-6').stop().animate().fadeOut();
      setTimeout(function(){
        $('#modal-7').stop().animate().fadeIn();
      }, 350);
      break;
    default:
      break;
  }
});

$(document).on('click', '.step-btn.visited', function(){
  let thisIndex = $(this).index();

  $('.step-btn').removeClass('active');
  $(this).addClass('active');
  $('.modal').stop().animate().fadeOut();
  $('.step-btn').addClass('off');
  setTimeout(function(){
    $('.modal').eq(thisIndex).stop().animate().fadeIn();
    $('.step-btn').removeClass('off');
  }, 350)
});

$('input[name="color"]').on('change', function(){
  $('.item-img').attr('src', $(this).data('src'));
});

$(document).on('click', '.call-search', function(){
  $('.search-form').addClass('active');
});
$(function() {
  var $container = $('.js-index');
  $container.find('');
});